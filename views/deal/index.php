<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Lead;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
    <?php if (\Yii::$app->user->can('createLead')) { ?>
	<p>
        <?= Html::a('Create Deal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php }	?>
	
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
			'leadid',
			 
		
			 /* [
				//'attribute' => 'leadid',
				'label' => 'leadid',
				'format' => 'raw',
				'value' => function($model){
					return array_keys(\Yii::$app->authManager->getLeadid($model->id))[0];					
				},
				'filter'=>Html::dropDownList('DealSearch[leadid]', $leadid, $leadids, ['class'=>'form-control']),
			],*/
			 
			 
			 
			 
			 
			 
			
				
		
			
            'name',
			'amount',
			
			
			
			

          ['class' => 'yii\grid\ActionColumn',
			'visibleButtons' => [
			'update' => (\yii::$app->user->can('updateDeal')),
			'delete' => (\yii::$app->user->can('deleteDeal')),
			],
			],
         ],
    ]); ?>
</div>
