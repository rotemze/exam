<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadid
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
	public $deal;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadid', 'name', 'amount'], 'required'],
            [['leadid', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 22],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadid' => 'Leadid',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	
			public static function getAmount(){
		 return 'amount';	
	}
	
	/*public static function getAllLeadids()
	{
		$leadids = self::getleadids();
		$leadids[-1] = 'All leadids';
		$leadids = array_reverse ( $leadids, true );
		return $leadids;	
	}
	
	public static function getleadids()
	{
		$leadidsObjects = Yii::$app->authManager->getleadids();
		$leadids = [];
		foreach($leadidsObjects as $id =>$leadidObj){
			$leadids[$id] = $leadidObj->name; 
		}
		return $leadids; 	
	}*/
		
	
}
